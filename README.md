INTRODUCTION
------------

Binary Language Switcher switches between two (2) languages. It is not tested
and not suitable for more.
Displays a link to the other available language, together with a prefix text if
supplied. Also has an 'inverse' mode, which instead of displaying the name of
the inactive language, it instead displays the name of the current language,
linked to the inactive one.
Crazy as it sound, the 'inverse' mode is/was a requirement on several projects.

REQUIREMENTS
------------

 * Requires a multilingual site with 2 languages.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

This module provides a language switcher block. All configuration is in the
block form. Sensible defaults are provided.

MAINTAINERS
-----------

 * Bill Seremetis (bserem)
