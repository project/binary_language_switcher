<?php

namespace Drupal\binary_language_switcher\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Language switcher' block.
 *
 * @Block(
 *   id = "binary_language_switcher",
 *   admin_label = @Translation("Binary Language Switcher"),
 *   category = @Translation("Multilingual")
 * )
 */
class BinaryLanguageBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Constructs an LanguageBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, PathMatcherInterface $path_matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $access = $this->languageManager->isMultilingual() ? AccessResult::allowed() : AccessResult::forbidden();
    return $access->addCacheTags(['config:configurable_language_list']);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = [];
    $route_name = $this->pathMatcher->isFrontPage() ? '<front>' : '<current>';
    $type = $this->getDerivativeId();
    $links = $this->languageManager->getLanguageSwitchLinks($type, Url::fromRoute($route_name));

    if ($config['binary_language_switcher_invert']) {
      $keys = array_keys($links->links);
      $temp = $links->links[$keys[0]]['title'];
      $links->links[$keys[0]]['title'] = $links->links[$keys[1]]['title'];
      $links->links[$keys[1]]['title'] = $temp;
    }

    if (isset($links->links)) {
      $build = [
        '#prefix' => $config['binary_language_switcher_prefix'],
        '#theme' => 'links__language_block',
        '#links' => $links->links,
        '#attributes' => [
          'class' => [
            "binary-language-switcher",
            "language-switcher-{$links->method_id}",
          ],
        ],
        '#attached' => [
          'library' => ['binary_language_switcher/binary_language_switcher'],
        ],
        '#set_active_class' => TRUE,
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['binary_language_switcher_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix Text'),
      '#description' => $this->t('Text to be displayed before the language link.'),
      '#default_value' => isset($config['binary_language_switcher_prefix']) ? $config['binary_language_switcher_prefix'] : $this->t('Switch to'),
    ];

    $form['binary_language_switcher_invert'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Inverse Mode'),
      '#description' => $this->t('Show active language instead of next language.'),
      '#default_value' => isset($config['binary_language_switcher_invert']) ? $config['binary_language_switcher_invert'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['binary_language_switcher_prefix'] = $values['binary_language_switcher_prefix'];
    $this->configuration['binary_language_switcher_invert'] = $values['binary_language_switcher_invert'];

  }

  /**
   * {@inheritdoc}
   *
   * @todo Make cacheable in https://www.drupal.org/node/2232375.
   * Copied from core language block.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
